// Base url
export const BASE_URL = 'http://localhost:8000/api';

// Client api
export const API_REGISTER = BASE_URL + '/client/auth/register';
export const API_URL_LOGIN_CLIENT = BASE_URL + '/client/auth/login';

// Admin api
// export const API_ADMIN_REGISTER = BASE_URL + 'admin/auth/register';
