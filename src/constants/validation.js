// User validation
export const EMAIL_MAXLENGTH = 50;
export const NAME_MAXLENGTH = 50;
export const PASSWORD_MINLENGTH = 6;
export const PASSWORD_MAXLENGTH = 30;
