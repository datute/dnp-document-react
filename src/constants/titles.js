// Title
export const REGISTER = 'Register';
export const EMAIL_ADDRESS = 'Email address';
export const NAME = 'Name';
export const PASSWORD = 'Password';
export const CONFIRM_PASSWORD = 'Confirm Password';
export const SUBMIT = 'Submit';
export const SUBMITTING = 'Submitting';
export const LOGIN = 'Login';
export const ADMIN_LOGIN = 'Admin Login';

// Source lsit
export const SOURCE_LIST = 'Source list';
export const SEARCH = 'Search';
export const SEARCHING = 'Searching';
export const SOURCE_TYPE = 'Source type';
