import {
    REGISTER_SUCCESS,
    REGISTER_FAILURE,
    LOGIN_USER_SUCCESS,
    LOGIN_USER_FAILURE
} from './types';
import { API_URL_LOGIN_CLIENT } from 'constants/api';
import { requestPost } from 'utils';
import { registerService } from 'services';

export const register = data => {
    return async dispatch => {
        function onSuccess(success) {
            dispatch({ type: REGISTER_SUCCESS, payload: success });
            return success;
        }
        function onError(error) {
            dispatch({ type: REGISTER_FAILURE, error });
            return error;
        }

        try {
            const success = await registerService.register(data);
            return onSuccess(success);
        } catch (error) {
            return onError(error);
        }
    };
};

export const logIn = data => {
    return async dispatch => {
        try {
            const success = await requestPost(API_URL_LOGIN_CLIENT, data);
            dispatch(logInSuccess(success.data));
            return success;
        } catch (error) {
            dispatch(logInFailure(error));
            return error;
        }
    };
};

export const logInSuccess = data => {
    return {
        type: LOGIN_USER_SUCCESS,
        payload: data
    };
};

export const logInFailure = error => {
    return {
        type: LOGIN_USER_FAILURE,
        error
    };
};
