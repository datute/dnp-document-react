import { combineReducers } from 'redux';
import registerReducer from './register-reducer';
import loginReducer from './login-reducer';
import { reducer as formReducer } from 'redux-form';

export default combineReducers({
    register: registerReducer,
    logIn: loginReducer,
    form: formReducer
});
