import {
    LOGIN,
    LOGIN_USER_SUCCESS,
    LOGIN_USER_FAILURE
} from '../actions/types';

const initialState = {
    userInfo: {
        data: null,
        loading: false
    }
};

const loginReducer = (state = initialState, action) => {
    switch (action.type) {
        case LOGIN: {
            return {
                ...state,
                userInfo: {
                    data: null,
                    error: null,
                    loading: false
                }
            };
        }
        case LOGIN_USER_SUCCESS:
            return {
                ...state,
                userInfo: {
                    data: action.payload.user,
                    error: null,
                    loading: false
                }
            };
        case LOGIN_USER_FAILURE:
            return {
                ...state,
                userInfo: {
                    data: null,
                    loading: false
                }
            };
        default:
            return state;
    }
};

export default loginReducer;
