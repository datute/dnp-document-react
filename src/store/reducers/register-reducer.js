import { REGISTER_SUCCESS, REGISTER_FAILURE } from '../actions/types';

const DEFAULT_STATE = {
    newUser: {
        data: null,
        error: null,
        loading: false
    }
};

export default function registerReducer(state = DEFAULT_STATE, action) {
    switch (action.type) {
        case REGISTER_SUCCESS:
            return {
                ...state,
                newUser: {
                    data: action.payload,
                    error: null,
                    loading: false
                }
            };
        case REGISTER_FAILURE:
            return {
                ...state,
                newUser: {
                    data: null,
                    error: action.payload,
                    loading: false
                }
            };
        default:
            return state;
    }
}
