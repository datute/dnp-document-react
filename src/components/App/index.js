import React, { Component } from 'react';
import { Router, Route, Switch } from 'react-router-dom';
import { createBrowserHistory } from 'history';
import HttpStatus from 'http-status-codes';
// Components
import { Header, Footer } from 'components/Elements';
import {
    HomePage,
    RegisterPage,
    LoginPage,
    AdminHomePage,
    AdminLoginPage,
    SourcePage
} from 'components/Pages';

import { NotificationContainer } from 'react-notifications';
import { NotificationManager } from 'react-notifications';
import { SubmissionError } from 'redux-form';
import { PrivateRoute } from 'components/Containers';
import { connect } from 'react-redux';
import { logIn } from 'store/actions';
const history = createBrowserHistory();

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            authenticated: false,
            isAdmin: false // TODO: when has admin struture
        };
    }

    verify = () => {
        let token = sessionStorage.getItem('token');
        if (token) {
            this.setState({
                authenticated: true
            });
        }
    };

    login = async values => {
        const request = await this.props.login(values);
        let dataResponse = request.data;
        if (request && request.status === HttpStatus.OK) {
            // get token
            let token = dataResponse.token;
            // save token to session
            sessionStorage.setItem('token', token);
            this.setState({
                authenticated: true
            });
            NotificationManager.success('login success');
        } else {
            let errorArray = {};
            let errorResponse = request.response;
            if (errorResponse && errorResponse.data) {
                if (
                    errorResponse.data.error &&
                    errorResponse.status === HttpStatus.UNPROCESSABLE_ENTITY
                ) {
                    errorArray = {
                        ...errorArray,
                        ...errorResponse.data.error.details
                    };
                } else {
                    errorArray['_error'] = errorResponse.data.error.message;
                }
            } else {
                // Server error
                if (request && request.message) {
                    errorArray['_error'] = request.message;
                }
            }

            throw new SubmissionError(errorArray);
        }
    };

    loginAdmin = async values => {
        // TODO: Login admin
    };

    logout = () => {
        // check exist token in session storage
        let token = sessionStorage.getItem('token');
        if (token) {
            sessionStorage.removeItem('token');
            this.setState({
                authenticated: false
            });
        }
    };

    componentDidMount() {
        this.verify();
    }

    render() {
        let { authenticated, isAdmin } = this.state;
        return (
            <React.Fragment>
                <Router history={history}>
                    <div id="page-container">
                        <Header
                            authenticated={authenticated}
                            logout={this.logout}
                            isAdmin={isAdmin}
                        ></Header>
                        <div id="content-wrap">
                            <Switch>
                                {/* Client */}
                                <PrivateRoute
                                    exact
                                    path="/"
                                    component={HomePage}
                                    authenticated={authenticated}
                                />
                                <Route path="/source" component={SourcePage} />
                                <Route
                                    path="/register"
                                    component={RegisterPage}
                                />
                                <Route
                                    path="/login"
                                    render={props => (
                                        <LoginPage
                                            {...props}
                                            onSubmit={this.login}
                                            location={props.location}
                                            authenticated={authenticated}
                                        />
                                    )}
                                />

                                {/* Admin */}
                                <Route
                                    exact
                                    path="/admin/"
                                    component={AdminHomePage}
                                />
                                <Route
                                    path="/admin/login"
                                    render={props => (
                                        <AdminLoginPage
                                            {...props}
                                            onSubmit={this.login}
                                            location={props.location}
                                            authenticated={authenticated}
                                        />
                                    )}
                                />
                            </Switch>
                        </div>

                        <Footer isAdmin={isAdmin}></Footer>
                    </div>
                </Router>
                <NotificationContainer />
            </React.Fragment>
        );
    }
}

const mapDispatchToProps = dispatch => {
    return {
        login: data => dispatch(logIn(data))
    };
};

export default connect(
    null,
    mapDispatchToProps
)(App);
