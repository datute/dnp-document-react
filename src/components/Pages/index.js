// Client page
export { default as HomePage } from './Client/HomePage';
export { default as RegisterPage } from './Client/RegisterPage';
export { default as LoginPage } from './Client/LoginPage';
export { default as SourcePage } from './Client/SourcePage';

// Admin page
export { default as AdminHomePage } from './Admin/AdminHomePage';
export { default as AdminLoginPage } from './Admin/AdminLoginPage';
