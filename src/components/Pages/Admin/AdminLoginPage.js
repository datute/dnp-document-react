import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import { ADMIN_LOGIN } from 'constants/titles';
import { Login } from 'components/Elements';

class AdminLoginPage extends Component {
    render() {
        const { authenticated, location } = this.props;

        const { from } = location.state || { from: { pathname: '/' } };
        // Redirect when already login
        if (from && authenticated) {
            return <Redirect to={from} />;
        }

        return <Login title={ADMIN_LOGIN} onSubmit={this.props.onSubmit} />;
    }
}

export default AdminLoginPage;
