import React from 'react';
import { HeaderComponer, ContentComponent } from 'components/Containers';

const HomePage = () => (
    <React.Fragment>
        <HeaderComponer />
        <ContentComponent />
    </React.Fragment>
);

export default HomePage;
