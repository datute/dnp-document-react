import React, { Component } from 'react';
import Card from 'react-bootstrap/Card';
import * as CONSTANTS from 'constants/titles';

import { Search, Table } from 'components/Elements';

const columns = [
    {
        name: '#',
        selector: 'index',
        sortable: true
    },
    {
        name: 'Name',
        selector: 'name',
        sortable: true
    },
    {
        name: 'C number',
        selector: 'c_number',
        sortable: true
    },
    {
        name: 'Source type',
        selector: 'source_type',
        sortable: true
    },
    {
        name: 'Description',
        selector: 'description',
        sortable: true
    },
    {
        name: 'Init path',
        selector: 'init_path',
        sortable: true
    },
    {
        name: 'Final path',
        selector: 'final_path',
        sortable: true
    },
    {
        name: 'Created user',
        selector: 'create_user',
        sortable: true
    },
    {
        name: 'Created at',
        selector: 'create_at',
        sortable: true
    }
];

const data = [
    {
        index: 1,
        name: 'AConan the Barbarian',
        c_number: '1982',
        source_type: 'Source type',
        description: 'description',
        init_path: 'init path',
        final_path: 'final path',
        create_user: 'create user',
        create_at: 'create at'
    },
    {
        index: 2,
        name: 'AConan the Barbarian',
        c_number: '1982',
        source_type: 'Source type',
        description: 'description',
        init_path: 'init path',
        final_path: 'final path',
        create_user: 'create user',
        create_at: 'create at'
    },
    {
        index: 3,
        name: 'AConan the Barbarian',
        c_number: '1982',
        source_type: 'Source type',
        description: 'description',
        init_path: 'init path',
        final_path: 'final path',
        create_user: 'create user',
        create_at: 'create at'
    }
];

class SourcePage extends Component {
    onSearch = values => {
        // TODO: Handle search
        console.log('Search list:', values);
    };

    render() {
        return (
            <React.Fragment>
                <div className="content-header">
                    <div className="container">
                        <div className="row mb-2">
                            <div className="col-sm-6">
                                <h1 className="m-0 text-dark">
                                    Source list <small></small>
                                </h1>
                            </div>
                            <div className="col-sm-6">
                                <ol className="breadcrumb float-sm-right">
                                    <li className="breadcrumb-item">
                                        <a href="#">Home</a>
                                    </li>
                                    <li className="breadcrumb-item">
                                        <a href="#">Layout</a>
                                    </li>
                                    <li className="breadcrumb-item active">
                                        Source list
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="content">
                    <div className="container">
                        <div className="row">
                            <div className="col-12">
                                <Search onSubmit={this.onSearch} />
                                <div>
                                    <Table columns={columns} data={data} />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

export default SourcePage;
