import React, { Component } from 'react';
import { connect } from 'react-redux';
import HttpStatus from 'http-status-codes';
import { SubmissionError } from 'redux-form';
import { NotificationManager } from 'react-notifications';
import { Register } from 'components/Elements';
import { register } from 'store/actions';
import { LOGIN_ROUTE } from 'constants/routes';

const mapStateToProps = state => {
    return {
        register: state.register.newUser
    };
};
const mapDispatchToProps = dispatch => {
    return {
        onRegister: user => dispatch(register(user))
    };
};

class RegisterPage extends Component {
    handleSubmit = async values => {
        const data = {
            email: values.email,
            name: values.name,
            password: values.password,
            confirm_password: values.confirm_password
        };

        const result = await this.props.onRegister(data);
        const response = result.response;
        if (result && result.status === HttpStatus.OK) {
            // Success
            if (result.data.message) {
                // Show notification
                NotificationManager.success(result.data.message);

                // Redirect
                this.props.history.push(LOGIN_ROUTE);
            }
        } else {
            // Show errors
            let errors = {};

            if (response && response.data) {
                // Validation failed 422
                if (
                    response.data.error &&
                    response.status === HttpStatus.UNPROCESSABLE_ENTITY
                ) {
                    let details = response.data.error.details;
                    errors = { ...details };
                }

                // Common error
                if (response.data.error && response.data.error.message) {
                    errors['_error'] = response.data.error.message;
                }
            } else {
                // Server error
                if (response && response.message) {
                    errors['_error'] = result.message;
                }
            }
            throw new SubmissionError(errors);
        }
    };

    render() {
        return (
            <main>
                <Register onSubmit={this.handleSubmit} />
            </main>
        );
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(RegisterPage);
