import React from 'react';

const Footer = ({ isAdmin }) =>
    !isAdmin ? (
        <div id="footer" className="py-2 bg-dark">
            <div className="container">
                <p className="m-0 text-center text-white">
                    Copyright © DDS Document Site 2019
                </p>
            </div>
        </div>
    ) : null;

export default Footer;
