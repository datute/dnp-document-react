import React from 'react';
import Form from 'react-bootstrap/Form';

const RenderField = props => {
    const {
        id,
        input,
        label,
        type,
        meta: { touched, error, warning }
    } = props;
    return (
        <React.Fragment>
            <input
                {...input}
                type={type}
                id={id}
                className="form-control"
                placeholder={label}
            />
            <label htmlFor={id}>{label}</label>
            {touched &&
                ((error && <span className="text-danger">{error}</span>) ||
                    (warning && (
                        <span className="text-warning">{warning}</span>
                    )))}
        </React.Fragment>
    );
};

const RenderFieldSearch = props => {
    const {
        id,
        input,
        label,
        type,
        meta: { touched, error, warning }
    } = props;
    return (
        <React.Fragment>
            <Form.Control {...input} type={type} placeholder={label} />
        </React.Fragment>
    );
};

export { RenderField, RenderFieldSearch };
