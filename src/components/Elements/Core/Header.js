import React from 'react';
import { Link } from 'react-router-dom';

const Header = ({ authenticated, logout, isAdmin }) => {
    return !isAdmin ? (
        <header>
            <nav className="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
                <Link className="navbar-brand" to="/">
                    DDS Document
                </Link>
                <button
                    className="navbar-toggler"
                    type="button"
                    data-toggle="collapse"
                    data-target="#navbarCollapse"
                    aria-controls="navbarCollapse"
                    aria-expanded="false"
                    aria-label="Toggle navigation"
                >
                    <span className="navbar-toggler-icon" />
                </button>
                <div className="collapse navbar-collapse" id="navbarCollapse">
                    <ul className="navbar-nav mr-auto">
                        <li className="nav-item active">
                            <Link className="nav-link" to="/">
                                Home <span className="sr-only">(current)</span>
                            </Link>
                        </li>
                        <li className="nav-item">
                            <Link className="nav-link" to="/posts">
                                Post
                            </Link>
                        </li>
                        <li className="nav-item">
                            <Link className="nav-link" to="/documents">
                                Document
                            </Link>
                        </li>
                    </ul>
                    {!authenticated ? (
                        <ul className="navbar-nav ml-auto">
                            <li className="nav-item">
                                <Link className="nav-link" to="/login">
                                    Login
                                </Link>
                            </li>
                            <li className="nav-item">
                                <Link className="nav-link" to="/register">
                                    Register
                                </Link>
                            </li>
                        </ul>
                    ) : (
                        <ul className="navbar-nav ml-auto">
                            <li className="nav-item">
                                <Link
                                    className="nav-link"
                                    to="#"
                                    onClick={logout}
                                >
                                    Logout
                                </Link>
                            </li>
                        </ul>
                    )}
                </div>
            </nav>
        </header>
    ) : null;
};

export default Header;
