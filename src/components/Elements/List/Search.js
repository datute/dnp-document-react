import React from 'react';
import { Field, reduxForm } from 'redux-form';
import * as CONSTANTS from 'constants/titles';
import Form from 'react-bootstrap/Form';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Button from 'react-bootstrap/Button';
import Dropdown from 'react-bootstrap/Dropdown';
import { RenderFieldSearch } from 'components/Elements';

const Search = props => {
    const { submitting, handleSubmit } = props;

    return (
        <Form onSubmit={handleSubmit}>
            <Row>
                <Col>
                    <Dropdown className="w-100">
                        <Dropdown.Toggle variant="primary" id="dropdown-basic">
                            {CONSTANTS.SOURCE_TYPE}
                        </Dropdown.Toggle>

                        <Dropdown.Menu>
                            <Dropdown.Item href="#/action-1">
                                Type 1
                            </Dropdown.Item>
                            <Dropdown.Item href="#/action-2">
                                Type 2
                            </Dropdown.Item>
                        </Dropdown.Menu>
                    </Dropdown>
                </Col>
                <Col>
                    <Field
                        name="search"
                        type="text"
                        placeholder="Search"
                        component={RenderFieldSearch}
                    />
                </Col>
                <Col className="text-left">
                    <Button
                        variant="primary"
                        type="submit"
                        disabled={submitting}
                    >
                        {submitting ? CONSTANTS.SEARCHING : CONSTANTS.SEARCH}
                    </Button>
                </Col>
            </Row>
        </Form>
    );
};

export default reduxForm({
    form: 'searchForm'
})(Search);
