import React from 'react';

const ListRow = props => {
    return (
        <div className="card card-primary card-outline">
            <div className="card-header">
                <h5 className="card-title m-0">Featured</h5>
            </div>
            <div className="card-body">
                <h6 className="card-title">Special title treatment</h6>

                <p className="card-text">
                    With supporting text below as a natural lead-in to
                    additional content.
                </p>
                <a href="#" className="btn btn-primary">
                    Go somewhere
                </a>
            </div>
        </div>
    );
};

export default ListRow;
