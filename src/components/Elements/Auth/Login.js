import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import { LOGIN } from 'constants/titles';
import { RenderField } from 'components/Elements';

class Login extends Component {
    render() {
        const { submitting, error, handleSubmit, title } = this.props;
        return (
            <form className="authentications" onSubmit={handleSubmit}>
                <div className="text-center mb-4">
                    <h1 className="h3 mb-3 font-weight-normal">{title}</h1>
                </div>
                <div className="form-label-group">
                    <Field
                        id="inputEmail"
                        label="Email"
                        name="email"
                        component={RenderField}
                        type="email"
                    />
                </div>
                <div className="form-label-group">
                    <Field
                        id="inputPassword"
                        label="Password"
                        name="password"
                        component={RenderField}
                        type="password"
                    />
                </div>
                {error && <span className="text-danger">{error}</span>}
                <div className="checkbox mb-3"></div>
                <button
                    className="btn btn-lg btn-primary btn-block"
                    type="submit"
                    disabled={submitting}
                >
                    {LOGIN}
                </button>
            </form>
        );
    }
}

export default reduxForm({
    form: 'login'
})(Login);
