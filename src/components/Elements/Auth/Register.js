import React from 'react';
import * as CONSTANTS from 'constants/titles';
import { Field, reduxForm } from 'redux-form';
import { RenderField } from 'components/Elements';

const Register = props => {
    const { submitting, handleSubmit, error } = props;

    return (
        <form className="authentications" onSubmit={handleSubmit}>
            <div className="text-center mb-4">
                <h1 className="h3 mb-3 font-weight-normal">
                    {CONSTANTS.REGISTER}
                </h1>
            </div>
            {error && <span className="text-danger">{error}</span>}
            <div className="form-label-group">
                <Field
                    name="email"
                    type="email"
                    label={CONSTANTS.EMAIL_ADDRESS}
                    id="inputEmail"
                    component={RenderField}
                />
            </div>
            <div className="form-label-group">
                <Field
                    name="name"
                    type="text"
                    label={CONSTANTS.NAME}
                    id="inputName"
                    component={RenderField}
                />
            </div>
            <div className="form-label-group">
                <Field
                    name="password"
                    type="password"
                    label={CONSTANTS.PASSWORD}
                    id="inputPassword"
                    component={RenderField}
                />
            </div>
            <div className="form-label-group">
                <Field
                    name="confirm_password"
                    type="password"
                    label={CONSTANTS.CONFIRM_PASSWORD}
                    id="inputConfirmPassword"
                    component={RenderField}
                />
            </div>
            <button
                className="btn btn-lg btn-primary btn-block"
                type="submit"
                disabled={submitting}
            >
                {submitting ? CONSTANTS.SUBMITTING : CONSTANTS.SUBMIT}
            </button>
        </form>
    );
};

export default reduxForm({
    form: 'registerForm'
})(Register);
