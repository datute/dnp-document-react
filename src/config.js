// API URL
export const API_URL = 'http://localhost:8000/api';

export const API_URL_REGISTER = API_URL + '/auth/register';
// sign in url api
export const API_URL_SIGNIN_CLIENT = API_URL + '/client/auth/login';
