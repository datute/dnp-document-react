import { API_REGISTER } from 'constants/api';
import { requestPost } from 'utils';

const register = async data => {
    return requestPost(API_REGISTER, data);
};

export default {
    register
};
