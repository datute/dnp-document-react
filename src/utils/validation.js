const required = value => (value ? undefined : `Field is required`);

const maxLength = max => value =>
    value && value.length > max
        ? `Must be ${max} characters or less`
        : undefined;

const minLength = min => value =>
    value && value.length < min
        ? `Must be ${min} characters or more`
        : undefined;

const minValue = min => value =>
    value && value < min ? `Must be at least ${min}` : undefined;

const maxValue = max => value =>
    value && value > max ? `Must be at greatest ${max}` : undefined;

const number = value =>
    value && isNaN(Number(value)) ? 'Must be a number' : undefined;

const email = value =>
    value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)
        ? 'Email is invalid'
        : undefined;

// Custom
const minLength8 = minLength(8);

export default {
    required,
    maxLength,
    minLength,
    minValue,
    maxValue,
    number,
    email,
    minLength8
};
