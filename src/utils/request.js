import axios from 'axios';
import { BASE_URL } from 'constants/api';

const axiosInstance = axios.create({
    baseURL: BASE_URL,
    header: {
        'Content-Type': 'application/json',
        Accept: 'application/json'
    }
});

// GET
const requestGet = (url, config = {}) => {
    return axiosInstance.get(url, config);
};

// HEAD
const requestHead = (url, config = {}) => {
    return axiosInstance.get(url, config);
};

// POST
const requestPost = (url, model, config = {}) => {
    return axiosInstance.post(url, model, config);
};

// PUT
const requestPut = (url, model, config = {}) => {
    return axiosInstance.put(url, model, config);
};

// Delete
const requestDelete = (url, config = {}) => {
    return axiosInstance.delete(url, config);
};

export {
    requestGet,
    requestHead,
    requestPost,
    requestPut,
    requestDelete,
    axiosInstance
};
